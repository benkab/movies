import React from 'react';
import Search from "./src/scenes/Search";
import Home from "./src/scenes/Home";
import Movie from "./src/scenes/Movie";
import Booking from "./src/scenes/Booking";
import Browse from "./src/scenes/Browse";
import Tickets from "./src/scenes/Tickets";
import SignUp from "./src/scenes/SignUp";
import Login from "./src/scenes/Login";
import Ticket from "./src/scenes/Ticket";
import { NavigationContainer } from '@react-navigation/native';
import { createSharedElementStackNavigator } from "react-navigation-shared-element";
import { store } from './src/redux'
import { Provider } from 'react-redux';
import { createStore } from 'redux'
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContent from './src/components/DrawerContent'
import { colors } from "./src/utils/colors";

const appStore = createStore(store)
const Stack = createSharedElementStackNavigator();

const animationSpec = {
    open: { animation: 'timing', config : { duration: 500 }},
    close: { animation: 'timing', config : { duration: 500 }}
}

const searchAnimationSpec = {
    open: { animation: 'timing', config : { duration: 200 }},
    close: { animation: 'timing', config : { duration: 200 }}
}

function RootStackScreen () {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
            }}
        >
            <Stack.Screen
                name="Home"
                component={Home}
                sharedElementsConfig={(route) => {
                    return [
                        { id: 'SearchContainer'}
                    ]
                }}
            />
            <Stack.Screen
                name="SignUp"
                component={SignUp}
            />
            <Stack.Screen
                name="Login"
                component={Login}
            />
            <Stack.Screen
                name="Search"
                component={Search}
                options={{
                    transitionSpec: searchAnimationSpec,
                    cardStyleInterpolator: ({ current: { progress }}) => {
                        return {
                            cardStyle: {
                                opacity: progress,
                            }
                        }
                    }
                }}
            />
            <Stack.Screen
                name="Movie"
                component={Movie}
                options={{
                    transitionSpec: animationSpec,
                    cardStyleInterpolator: ({ current: { progress }}) => {
                        return {
                            cardStyle: {
                                opacity: progress,
                            }
                        }
                    }
                }}
                sharedElementsConfig={(route) => {
                    const { movie } = route.params
                    return [
                        { id: `movie-item-${movie.key}`}
                    ]
                }}
            />
            <Stack.Screen
                name="Ticket"
                component={Ticket}
                options={{
                    transitionSpec: animationSpec,
                    cardStyleInterpolator: ({ current: { progress }}) => {
                        return {
                            cardStyle: {
                                opacity: progress,
                            }
                        }
                    }
                }}
                sharedElementsConfig={(route) => {
                    const { movie } = route.params
                    return [
                        { id: `ticket-item-${movie.key}`}
                    ]
                }}
            />
            <Stack.Screen
                name="Booking"
                component={Booking}
                options={{
                    transitionSpec: searchAnimationSpec,
                    cardStyleInterpolator: ({ current: { progress }}) => {
                        return {
                            cardStyle: {
                                opacity: progress,
                            }
                        }
                    }
                }}
            />
            <Stack.Screen
                name="Browse"
                component={Browse}
                options={{
                    transitionSpec: animationSpec,
                    cardStyleInterpolator: ({ current: { progress }}) => {
                        return {
                            cardStyle: {
                                opacity: progress,
                            }
                        }
                    }
                }}
            />
        </Stack.Navigator>
    );
}

const Drawer = createDrawerNavigator();

const inactiveColor = colors.semiDarkGray
const activeColor = colors.yellowColor

export default function App() {
  return (
      <Provider store={appStore}>
          <NavigationContainer>
              <Drawer.Navigator
                  drawerContent={props => <DrawerContent {...props} />}
                  initialRouteName="Home"
                  screenOptions={{
                      headerShown: false,
                  }}
              >
                  <Drawer.Screen name="HomeDrawer" component={RootStackScreen} />
                  <Drawer.Screen
                      name="Tickets"
                      component={Tickets}
                      options={{
                          transitionSpec: searchAnimationSpec,
                          cardStyleInterpolator: ({ current: { progress }}) => {
                              return {
                                  cardStyle: {
                                      opacity: progress,
                                  }
                              }
                          }
                      }}
                  />
              </Drawer.Navigator>
          </NavigationContainer>
      </Provider>
  );
}
