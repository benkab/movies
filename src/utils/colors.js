export const colors = {
    mainColor: '#1D1D27',
    statusBarColor: '#14141c',
    lightGray: '#DDDDDD',
    darkGray: '#51515e',
    darkBackgroundColor: '#14141c',
    yellowColor: '#E89B38',
    darkYellowColor: '#927c60',
    borderColor: '#22222b',
    roseColor: '#D11F50',
    semiDarkGray: '#88888b',
    ticketBorderGray: '#3e3e45',
    greenColor: '#2ca084',
}
