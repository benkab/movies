export default [
    {
        key: '1',
        title: 'Angel has fallen',
        year: '2019',
        category: 'Action',
        rate: 4.5,
        image: require('./../assets/images/movie-4.jpg'),
        summary: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam asperiores atque beatae commodi consectetur, eaque error eum ipsa, itaque laboriosam laudantium natus officiis optio porro quis reprehenderit sint velit"
    },
    {
        key: '2',
        title: 'World War 3',
        year: '2018',
        category: 'Comedy',
        rate: 4.2,
        image: require('./../assets/images/movie-5.jpg'),
        summary: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam asperiores atque beatae commodi consectetur, eaque error eum ipsa, itaque laboriosam laudantium natus officiis optio porro quis reprehenderit sint velit"
    },
    {
        key: '3',
        title: 'Angel has fallen',
        year: '2019',
        category: 'Action',
        rate: 4.5,
        image: require('./../assets/images/movie-3.jpg'),
        summary: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam asperiores atque beatae commodi consectetur, eaque error eum ipsa, itaque laboriosam laudantium natus officiis optio porro quis reprehenderit sint velit"
    },
    {
        key: '4',
        title: 'World War 3',
        year: '2018',
        category: 'Comedy',
        rate: 4.2,
        image: require('./../assets/images/movie-2.jpg'),
        summary: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam asperiores atque beatae commodi consectetur, eaque error eum ipsa, itaque laboriosam laudantium natus officiis optio porro quis reprehenderit sint velit"
    },
    {
        key: '5',
        title: 'Angel has fallen',
        year: '2019',
        category: 'Action',
        rate: 4.5,
        image: require('./../assets/images/movie-6.jpg'),
        summary: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam asperiores atque beatae commodi consectetur, eaque error eum ipsa, itaque laboriosam laudantium natus officiis optio porro quis reprehenderit sint velit"
    },
    {
        key: '6',
        title: 'World War 3',
        year: '2018',
        category: 'Comedy',
        rate: 4.2,
        image: require('./../assets/images/movie-1.jpg'),
        summary: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam asperiores atque beatae commodi consectetur, eaque error eum ipsa, itaque laboriosam laudantium natus officiis optio porro quis reprehenderit sint velit"
    }
]
