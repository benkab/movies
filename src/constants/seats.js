export default [
    {
        id: 0,
        description: 'A0',
        selected: false,
        reserved: false,
        disabled: true
    },
    {
        id: 102,
        description: 'A0-Bis',
        selected: false,
        reserved: false,
        disabled: true
    },
    {
        id: 1,
        description: 'A1',
        selected: false,
        reserved: true,
        disabled: false
    },
    {
        id: 2,
        description: 'A2',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 3,
        description: 'A3',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 4,
        description: 'A4',
        selected: false,
        reserved: true,
        disabled: false
    },
    {
        id: 5,
        description: 'A5',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 6,
        description: 'A6',
        selected: false,
        reserved: false,
        disabled: true
    },
    {
        id: 7,
        description: 'A7',
        selected: false,
        reserved: false,
        disabled: true
    },
    {
        id: 8,
        description: 'A8',
        selected: false,
        reserved: false,
        disabled: true
    },
    {
        id: 9,
        description: 'A9',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 10,
        description: 'A10',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 11,
        description: 'A11',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 12,
        description: 'A12',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 13,
        description: 'A13',
        selected: false,
        reserved: true,
        disabled: false
    },
    {
        id: 14,
        description: 'A14',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 15,
        description: 'A15',
        selected: false,
        reserved: true,
        disabled: false
    },
    {
        id: 16,
        description: 'A16',
        selected: false,
        reserved: false,
        disabled: true
    },
    {
        id: 17,
        description: 'A17',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 18,
        description: 'A18',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 19,
        description: 'A19',
        selected: false,
        reserved: true,
        disabled: false
    },
    {
        id: 20,
        description: 'A20',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 21,
        description: 'A21',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 22,
        description: 'A22',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 23,
        description: 'A23',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 24,
        description: 'A24',
        selected: false,
        reserved: true,
        disabled: false
    },
    {
        id: 25,
        description: 'A25',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 26,
        description: 'A26',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 27,
        description: 'A27',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 28,
        description: 'A28',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 29,
        description: 'A29',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 30,
        description: 'A30',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 31,
        description: 'A31',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 32,
        description: 'A32',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 33,
        description: 'A33',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 34,
        description: 'A34',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 35,
        description: 'A35',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 36,
        description: 'A36',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 237,
        description: 'A237',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 38,
        description: 'A38',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 39,
        description: 'A39',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 40,
        description: 'A40',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 41,
        description: 'A41',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 42,
        description: 'A42',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 43,
        description: 'A43',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 44,
        description: 'A44',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 45,
        description: 'A45',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 46,
        description: 'A46',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 47,
        description: 'A47',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 48,
        description: 'A48',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 49,
        description: 'A49',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 50,
        description: 'A50',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 51,
        description: 'A51',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 52,
        description: 'A52',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 53,
        description: 'A53',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 54,
        description: 'A54',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 55,
        description: 'A55',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 56,
        description: 'A56',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 57,
        description: 'A57',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 58,
        description: 'A58',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 59,
        description: 'A59',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 60,
        description: 'A60',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 61,
        description: 'A61',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 53,
        description: 'A53',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 54,
        description: 'A54',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 55,
        description: 'A55',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 56,
        description: 'A56',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 57,
        description: 'A57',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 58,
        description: 'A58',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 59,
        description: 'A59',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 60,
        description: 'A60',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 61,
        description: 'A61',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 60,
        description: 'A60',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 61,
        description: 'A61',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 53,
        description: 'A53',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 54,
        description: 'A54',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 55,
        description: 'A55',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 56,
        description: 'A56',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 57,
        description: 'A57',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 33,
        description: 'A33',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 34,
        description: 'A34',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 35,
        description: 'A35',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 36,
        description: 'A36',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 237,
        description: 'A237',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 38,
        description: 'A38',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 39,
        description: 'A39',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 40,
        description: 'A40',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 41,
        description: 'A41',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 42,
        description: 'A42',
        selected: false,
        reserved: false,
        disabled: false
    },
    {
        id: 43,
        description: 'A43',
        selected: false,
        reserved: false,
        disabled: false
    },
]
