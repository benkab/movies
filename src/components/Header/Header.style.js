import { StyleSheet, Platform } from "react-native";

export const styles = StyleSheet.create({
    container: {
        paddingTop: Platform.OS === 'ios' ? 16 : 48,
        paddingHorizontal: 16,
        paddingBottom: 16
    },
    topSectionContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
});
