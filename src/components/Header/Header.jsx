import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { styles } from "./Header.style";
import { Icon } from 'react-native-elements'
import { colors } from "../../utils/colors";

const Header = ({ children, navigation }) => {

    return (
        <View style={styles.container}>
            <View style={styles.topSectionContainer}>
                <TouchableOpacity
                    style={{
                        width: 24,
                        height: 16,
                        position: 'relative',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        flexDirection: 'row'
                    }}
                    onPress={() => {
                        navigation.openDrawer();
                    }}
                >
                    <View style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        width: 24,
                        height: 2.2,
                        backgroundColor: colors.yellowColor
                    }}/>
                    <View style={{
                        position: 'absolute',
                        bottom: 0,
                        left: 0,
                        width: 24,
                        height: 2.2,
                        backgroundColor: colors.lightGray
                    }}/>
                    <View style={{
                        width: 16,
                        height: 2,
                        backgroundColor: colors.lightGray
                    }}/>
                    <View style={{
                        width: 6,
                        height: 2,
                        backgroundColor: colors.yellowColor
                    }}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Icon
                        name='account-circle'
                        type='material'
                        color={colors.lightGray}
                        size={32}
                    />
                </TouchableOpacity>
            </View>
            {children}
        </View>
    );
}

export default Header;
