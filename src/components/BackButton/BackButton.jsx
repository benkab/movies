import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import { styles } from "./BackButton.style";
import {Icon} from "react-native-elements";
import {colors} from "../../utils/colors";
import * as Animatable from 'react-native-animatable';

const ANIMATION_DURATION = 600
const DELAY = 400

const show = {
    0: {
        opacity: 0,
    },
    1: {
        opacity: 1,
    }
}

const BackButton = ({ navigation, priorAction, hasPriorAction }) => {
    return (
        <Animatable.View
            style={styles.backButtonContainer}
            animation={show}
            duration={ANIMATION_DURATION}
            delay={DELAY}
        >
            <TouchableOpacity
                style={styles.backButton}
                onPress={() => {
                    if (hasPriorAction) {
                        priorAction()
                    }
                    navigation.goBack()
                }}
            >
                <Icon
                    name='corner-up-left'
                    type='feather'
                    color={colors.lightGray}
                    size={28}
                />
            </TouchableOpacity>
        </Animatable.View>
    );
}

export default BackButton;
