import { StyleSheet, Platform, Dimensions } from "react-native";

const { height } = Dimensions.get('screen')
const condition = height > 750 ? 48 : 40
export const styles = StyleSheet.create({
    backButtonContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        paddingHorizontal: 16,
        position: 'absolute',
        top: Platform.OS === 'ios' ? 16 : condition,
        zIndex: 99,
    },
    backButton: {
        marginLeft: -4
    },
});
