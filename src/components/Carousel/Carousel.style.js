import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get('screen')
const MAIN_WIDTH = width - 180
const MAIN_HEIGHT = height / 2.8

export const styles = StyleSheet.create({
    container: {
        position: 'relative',
        height: MAIN_HEIGHT + 95,
        marginBottom: 16,
    },
    carouselDetails: {
        height: 90,
        overflow: 'hidden',
    },
    imageOutsideContainer: {
        height: MAIN_HEIGHT,
        width: MAIN_WIDTH,
        position: 'absolute',
        left: -MAIN_WIDTH / 5,
        top: 0,
        borderRadius: 20
    },
    image: {
        height: MAIN_HEIGHT,
        width: MAIN_WIDTH,
        borderRadius: 20
    },
});
