import React, {useCallback, useEffect, useRef, useState} from 'react';
import {View, FlatList, TouchableOpacity, Image, Animated} from 'react-native';
import { styles } from "./Carousel.style";
import MovieCard from '../../components/MovieCard'
import movies from "../../constants/movies";
import { FlingGestureHandler, Directions, State } from 'react-native-gesture-handler'
import { SharedElement } from "react-native-shared-element";
import * as Animatable from 'react-native-animatable';

const VISIBLE_ITEMS = 5
const fadeInLeft = {
    0: {
        opacity: 0,
        translateX: 100
    },
    1: {
        opacity: 1,
        translateX: 0
    }
}

const Carousel = ({ navigation }) => {
    const [imageLoaded, setImageLoaded] = useState(false)
    const scrollXIndex = useRef(new Animated.Value(0)).current
    const scrollXAnimated = useRef(new Animated.Value(0)).current
    const [index, setIndex] = useState(0)
    const setActiveIndex = useCallback((activeIndex) => {
        setIndex(activeIndex)
        scrollXIndex.setValue(activeIndex)
    })

    const _onLoad = () => {
        if (!imageLoaded) setImageLoaded(true)
    }

    useEffect(() => {
        Animated.spring(scrollXAnimated, {
            toValue: scrollXIndex,
            useNativeDriver: true
        }).start();
    })

    return (
        <FlingGestureHandler
            key='left'
            direction={Directions.LEFT}
            onHandlerStateChange={event => {
                if (event.nativeEvent.state === State.END) {
                    if (index === (movies.length - 1)) return;
                    setActiveIndex(index + 1)
                }
            }}
        >
            <FlingGestureHandler
                key='right'
                 direction={Directions.RIGHT}
                 onHandlerStateChange={event => {
                     if (event.nativeEvent.state === State.END) {
                         if (index === 0) return;
                         setActiveIndex(index - 1)
                     }
                 }}
            >
                <View style={styles.container}>
                    <View style={styles.carouselDetails}>
                        {movies.map((movie, index) => {
                            return (
                                <MovieCard
                                    key={index}
                                    movie={movie}
                                    scrollXAnimated={scrollXAnimated}
                                />
                            );
                        })}
                    </View>
                    <Animatable.View
                        animation={fadeInLeft}
                        duration={400}
                        style={{
                            flex: 1
                        }}
                    >
                        <FlatList
                            data={movies}
                            keyExtractor={(item) => item.key}
                            horizontal
                            inverted
                            contentContainerStyle={{
                                flex: 1,
                                justifyContent: 'center'
                            }}
                            CellRendererComponent={({item, index, children, style, ...props}) => {
                                const newStyle = [style, {
                                    zIndex: movies.length - index
                                }]
                                return (
                                    <View style={newStyle} index={index} {...props}>
                                        {children}
                                    </View>
                                )
                            }}
                            scrollEnabled={false}
                            removeClippedSubviews={false}
                            decelerationRate={Platform.OS === 'ios' ? 0 : 0.9}
                            renderToHardwareTextureAndroid
                            snapToAlignment='start'
                            scrollEventThrottle={16}
                            renderItem={({item, index : i}) => {
                                const inputRange = [i - 1, i, i + 1]
                                const translateX = scrollXAnimated.interpolate({
                                    inputRange,
                                    outputRange: [50, 0, -100]
                                })
                                const scale = scrollXAnimated.interpolate({
                                    inputRange,
                                    outputRange: [.85, 1, 1.3]
                                })
                                const opacity = scrollXAnimated.interpolate({
                                    inputRange,
                                    outputRange: [1 - 1 / VISIBLE_ITEMS, 1, 0]
                                })
                                return (
                                    <Animated.View
                                        style={[styles.imageOutsideContainer, {
                                            opacity,
                                            transform: [{
                                                translateX
                                            }, { scale }],

                                        }]}
                                    >
                                        <TouchableOpacity style={styles.image} activeOpacity={0.9} onPress={() => {
                                            navigation.navigate('Movie', {
                                                movie: movies[index]
                                            })
                                        }}>
                                            <SharedElement id={`movie-item-${item.key}`}>
                                                <Image source={item.image} style={styles.image} />
                                            </SharedElement>
                                        </TouchableOpacity>
                                    </Animated.View>
                                )
                            }}
                            showsHorizontalScrollIndicator={false}
                        />
                    </Animatable.View>
                </View>
            </FlingGestureHandler>
        </FlingGestureHandler>
    );
}

export default Carousel;
