import { StyleSheet } from "react-native";
import { colors } from "../../utils/colors";

export const styles = StyleSheet.create({
    container: {
        marginBottom: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: colors.borderColor
    },
    image: {
        height: 120,
        width: 100,
        borderRadius: 0,
        marginRight: 24
    },
    title: {
        fontSize: 18,
        fontWeight: '700',
        color: colors.lightGray,
        paddingTop: 16
    },
    year: {
        fontSize: 13,
        fontWeight: '600',
        color: colors.darkGray,
        paddingVertical: 8,
    },
    ratingContainer: {
        paddingVertical: 8
    }
});
