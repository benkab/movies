import React from 'react';
import {Image, View, Text, TouchableOpacity} from 'react-native';
import { styles } from "./Card.style";
import StarRating from "react-native-star-rating";
import { colors } from "../../utils/colors";
import * as Animatable from 'react-native-animatable';

const fadeInBottom = {
    0: {
        opacity: 0,
        translateY: 100
    },
    1: {
        opacity: 1,
        translateY: 0
    }
}

const Card = ({ navigation, movie, index }) => {
    const { title, year, category, rate, image } = movie
    return (
        <Animatable.View
            style={styles.container}
            animation={fadeInBottom}
            duration={400}
            delay={400 * index}
        >
            <TouchableOpacity activeOpacity={0.9} onPress={() => {
                navigation.navigate('Movie', {
                    movie: movie
                })
            }}>
                <Image source={image} style={styles.image} />
            </TouchableOpacity>
            <View>
                <Text style={styles.title}>{title}</Text>
                <View style={styles.ratingContainer}>
                    <StarRating
                        disabled={false}
                        maxStars={5}
                        rating={rate}
                        starSize={16}
                        fullStarColor={colors.yellowColor}
                        selectedStar={(rating) => {}}
                        containerStyle={{
                            minWidth: 100,
                            maxWidth: 100,
                        }}
                    />
                </View>
                <Text style={styles.year}>{category} | Romance | {year}</Text>
            </View>
        </Animatable.View>
    );
}

export default Card;
