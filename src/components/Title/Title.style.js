import { StyleSheet } from "react-native";
import { colors } from "../../utils/colors";

export const styles = StyleSheet.create({
    container: {
        paddingTop: 8,
        paddingHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        marginBottom: 8
    },
    titleDescription: {
        fontSize: 20,
        color: colors.lightGray,
        fontWeight: '600'
    },
    titleLink: {
        fontSize: 14,
        color: colors.darkGray,
        fontWeight: '600'
    }
});
