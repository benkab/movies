import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { styles } from "./Title.style";

const Title = ({ title, navigation }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.titleDescription}>{title}</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Browse')}>
                <Text style={styles.titleLink}>View all</Text>
            </TouchableOpacity>
        </View>
    );
}

export default Title;
