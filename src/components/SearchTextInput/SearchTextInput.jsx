import React from 'react';
import { View, TextInput, Dimensions } from 'react-native';
import { styles } from "./SearchTextInput.style";
import { colors } from "../../utils/colors";

const { width } = Dimensions.get('screen')

const SearchTextInput = ({ fullWidth, animateable, onSearching, navigation }) => {

    const searchInputRef = React.useRef()
    React.useEffect(() => {
        if (navigation) {
            const unsubscribe = navigation.addListener('focus', () => {
                searchInputRef.current.focus();
            });

            return unsubscribe;
        }
    }, [navigation]);

    return (
        <View style={styles.searchContainer}>
            <TextInput
                placeholder="Search"
                placeholderTextColor={colors.darkGray}
                style={[styles.textInput, {
                    width: fullWidth ? (width - 70) : (width - 88)
                }]}
                onFocus={() => {
                    if (animateable) {
                        onSearching()
                    }
                }}
                ref={searchInputRef}
            />
        </View>
    );
}

export default SearchTextInput;
