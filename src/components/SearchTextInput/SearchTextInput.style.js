import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../utils/colors";

const { width } = Dimensions.get('screen')

export const styles = StyleSheet.create({
    searchContainer: {
        marginTop: 16,
        backgroundColor: colors.darkBackgroundColor,
        height: 42,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 8,
        flexDirection: 'row'
    },
    textInput: {
        marginLeft: 4,
        height: 40,
        width: width - 70,
        fontSize: 14,
        fontWeight: '600',
        color: colors.lightGray
    }
});
