import { StyleSheet } from "react-native";
import { colors } from "../../utils/colors";

export const styles = StyleSheet.create({
    container: {
        marginTop: 16,
        paddingHorizontal: 0,
        marginBottom: 0,
        backgroundColor: colors.darkBackgroundColor
    },
});
