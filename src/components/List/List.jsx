import React from 'react';
import { View } from 'react-native';
import { styles } from "./List.style";
import movies from "../../constants/movies";
import Card from "../Card";

const List = ({ navigation }) => {
    return (
        <View style={styles.container}>
            {movies.map((movie, index) => {
                return (
                    <Card
                        movie={movie}
                        key={index}
                        navigation={navigation}
                        index={index + 1}
                    />
                )
            })}
        </View>
    );
}

export default List;
