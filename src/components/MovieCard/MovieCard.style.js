import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../utils/colors";

const { width, height } = Dimensions.get('screen')
const MAIN_WIDTH = width - 32

export const styles = StyleSheet.create({
    container: {
        width: MAIN_WIDTH,
        paddingLeft: 16,
        height: 90,
        paddingVertical: 16,
    },
    detailsContainer: {
        paddingHorizontal: 12
    },
    movieTitle: {
        fontSize: 22,
        fontWeight: '700',
        color: colors.lightGray
    },
    detailsBottomContainer: {
        paddingTop: 2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: MAIN_WIDTH - 24,
        alignItems: 'center'
    },
    year: {
        fontSize: 13,
        fontWeight: '600',
        color: colors.darkGray
    },
    starts: {
        fontSize: 13,
        fontWeight: '600',
        color: colors.lightGray,
        paddingLeft: 2,
    },
    starsContainer: {
        flexDirection: 'row',
        backgroundColor: colors.darkBackgroundColor,
        paddingVertical: 4,
        paddingHorizontal: 6,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: colors.darkGray,
    },
});
