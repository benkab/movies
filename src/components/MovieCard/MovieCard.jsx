import React, { useState } from 'react';
import { Text, View, Animated, TouchableOpacity, ActivityIndicator } from 'react-native';
import { styles } from "./MovieCard.style";
import { Icon } from 'react-native-elements'
import { colors } from "../../utils/colors";

const OVERFLOW_HEIGHT = 90

const MovieCard = ({ movie, scrollXAnimated }) => {
    const inputRange = [-1, 0, 1]
    const translateY = scrollXAnimated.interpolate({
        inputRange,
        outputRange: [OVERFLOW_HEIGHT, 0, -OVERFLOW_HEIGHT]
    })

    const { title, year, category, rate } = movie
    return (
        <View style={styles.container}>
            <Animated.View style={[styles.detailsContainer, {
                transform: [{ translateY }]
            }]}>
                <Text style={styles.movieTitle}>{title}</Text>
                <View style={styles.detailsBottomContainer}>
                    <Text style={styles.year}>{category} | Romance | {year}</Text>
                    <View style={styles.starsContainer}>
                        <Icon
                            name='star'
                            type='material'
                            color={colors.yellowColor}
                            size={16}
                        />
                        <Text style={styles.starts}>{rate}</Text>
                    </View>
                </View>
            </Animated.View>
        </View>
    );
}

export default MovieCard;
