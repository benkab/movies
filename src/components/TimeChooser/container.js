import { connect } from 'react-redux'
import TimeChooser from './TimeChooser'
import lifecycle from './../../utils/lifecycle'
import { setTime } from './../../redux/global/actions'

export const mapStateToProps = ({ global, ...state }) => {
    const { time } = global
    return ({
        ...state,
        selectedTime: time,
    })
}

export const mergeProps = (state, { dispatch }, props) => ({
    ...state,
    ...props,
    didMount() {},
    onSelectItem (date) {
        dispatch(setTime(date))
    }

})

export default
connect(mapStateToProps, null, mergeProps)
(lifecycle({ didMount: 'didMount' })(TimeChooser))
