import React from 'react';
import {
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    Dimensions
} from 'react-native';
import { styles } from "./TimeChooser.style";
import { colors } from "../../utils/colors";
import { Picker } from '@react-native-picker/picker';

const { height } = Dimensions.get('screen')

const times = [
    {
        time: '11:30 AM',
    },
    {
        time: '12:30 AM',
    },
    {
        time: '1:30 PM',
    },
    {
        time: '2:30 PM',
    },
    {
        time: '3:45 PM',
    },
    {
        time: '20:00 AM',
    }
]

const TimeChooserInlineItem = ({ item, selectedTime, onSelectItem }) => {
    const { time } = item
    const selected = item === selectedTime
    return (
        <TouchableOpacity
            style={[styles.inlineItemContainer, {
                backgroundColor: selected ? colors.lightGray : 'transparent',
            }]}
            onPress={() => {
                onSelectItem(item)
            }}
        >
            <Text style={[styles.time, {
                color: selected ? colors.darkBackgroundColor : colors.darkGray
            }]}>{time}</Text>
        </TouchableOpacity>
    );
}

const TimeChooser = ({ selectedTime, onSelectItem }) => {
    return (
        <View>
            <ScrollView
                style={styles.inlineContainer}
                showsHorizontalScrollIndicator={false}
                horizontal
                scrollEventThrottle={16}
            >
                {times.map((item, index) => {
                    return (
                        <TimeChooserInlineItem
                            key={index}
                            item={item}
                            index={index}
                            selectedTime={selectedTime}
                            onSelectItem={onSelectItem}
                        />
                    )
                })}
            </ScrollView>
        </View>
    );
}

export default TimeChooser;
