import { StyleSheet } from "react-native";
import { colors } from "../../utils/colors";

export const styles = StyleSheet.create({
    container: {
        marginTop: 24,
        height: 170,
        overflow: 'hidden'
    },
    itemContainer: {
        borderRadius: 4,
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 12,
        borderTopWidth: 1,
        borderTopColor: colors.borderColor
    },
    time: {
        fontSize: 15,
        fontWeight: '700',
        color: colors.lightGray,
    },
    circle: {
        backgroundColor: colors.darkBackgroundColor,
        borderRadius: 100
    },
    leftCircle: {
        width: 8,
        height: 8,
        position: 'absolute',
        bottom: '50%',
        left: -4,
        backgroundColor: colors.mainColor,
    },
    rightCircle: {
        width: 8,
        height: 8,
        position: 'absolute',
        bottom: '50%',
        right: -4,
        backgroundColor: colors.mainColor,
    },
    inlineContainer: {
        paddingLeft: 24,
        paddingTop: 16
    },
    inlineItemContainer: {
        paddingVertical: 8,
        paddingHorizontal: 16,
        marginRight: 16,
        borderWidth: 1,
        borderColor: colors.darkGray,
        borderRadius: 6
    }
});
