import React from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { styles } from "./DateChooser.style";
import { colors } from "../../utils/colors";
import { getDates } from "../../utils/helpers";
import moment from 'moment'

const DateChooserItem = ({ date, selectedDate, onSelectDate }) => {
    const selected = date === selectedDate
    return (
        <TouchableOpacity
            onPress={() => {
                onSelectDate(date)
            }}
            style={[styles.itemContainer, {
            backgroundColor: selected ? colors.yellowColor : 'rgba(81, 81, 94, 0.5)',
        }]}>
            <View style={styles.circle} />
            <Text style={[styles.date, {
                color: selected ? colors.mainColor : colors.lightGray,
            }]}>{moment(new Date(date)).format('ddd')}</Text>
            <Text style={[styles.day, {
                color: selected ? colors.mainColor : colors.lightGray
            }]}>{moment(new Date(date)).format('DD')}</Text>
            <View style={[styles.circle, styles.leftCircle]} />
            <View style={[styles.circle, styles.rightCircle]} />
        </TouchableOpacity>
    );
}

const DateChooser = ({ selectedDate, onSelectDate }) => {
    const dates = getDates(new Date(), 10)
    return (
        <View style={styles.container}>
            <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
                style={{
                    paddingHorizontal: 16
                }}
            >
                {dates.map((item, index) => {
                    return (
                        <DateChooserItem
                            key={index}
                            date={item}
                            selectedDate={selectedDate}
                            onSelectDate={onSelectDate}
                        />
                    )
                })}
            </ScrollView>
        </View>
    );
}

export default DateChooser;
