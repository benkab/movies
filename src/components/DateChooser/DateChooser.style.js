import { StyleSheet } from "react-native";
import { colors } from "../../utils/colors";

export const styles = StyleSheet.create({
    container: {
        marginTop: 48,
    },
    itemContainer: {
        width: 68,
        height: 90,
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginRight: 10,
        backgroundColor: 'rgba(81, 81, 94, 0.5)',
        paddingVertical: 10,
        overflow: 'hidden'

    },
    date: {
        fontSize: 16,
        fontWeight: '600',
        color: colors.lightGray,
    },
    day: {
        fontSize: 22,
        fontWeight: '700',
        color: colors.lightGray,
    },
    circle: {
        width: 10,
        height: 10,
        backgroundColor: colors.darkBackgroundColor,
        borderRadius: 100
    },
    leftCircle: {
        width: 8,
        height: 8,
        position: 'absolute',
        bottom: '25%',
        left: -4,
        backgroundColor: colors.mainColor,
    },
    rightCircle: {
        width: 8,
        height: 8,
        position: 'absolute',
        bottom: '25%',
        right: -4,
        backgroundColor: colors.mainColor,
    }
});
