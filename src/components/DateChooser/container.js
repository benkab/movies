import { connect } from 'react-redux'
import DateChooser from './DateChooser'
import lifecycle from './../../utils/lifecycle'
import { setDate } from './../../redux/global/actions'

export const mapStateToProps = ({ global, ...state }) => {
    const { date } = global
    return ({
        ...state,
        selectedDate: date,
    })
}

export const mergeProps = (state, { dispatch }, props) => ({
    ...state,
    ...props,
    didMount() {},
    onSelectDate (date) {
        dispatch(setDate(date))
    }

})

export default connect(mapStateToProps, null, mergeProps)(lifecycle({ didMount: 'didMount' })(DateChooser))
