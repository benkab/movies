import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../utils/colors";

const { height } = Dimensions.get('screen')

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.mainColor,
        justifyContent: 'space-between'
    },
    mainContainer: {
        height: height / 1.5,
        backgroundColor: colors.darkBackgroundColor,
    },
    drawerItem: {
        // borderBottomWidth: 1,
        // borderBottomColor: colors.borderColor,
        // marginHorizontal: 0,
        // paddingHorizontal: 16,
    },
    label: {
        fontWeight: '600',
        // color: colors.darkGray,
        padding: 0,
        margin: 0
    }
});
