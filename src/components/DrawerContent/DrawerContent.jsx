import React from 'react';
import {
    DrawerItem,
    DrawerContentScrollView
} from '@react-navigation/drawer';
import {Text, View, SafeAreaView} from "react-native";
import AppStatusBar from "../../partials/AppStatusBar";
import { styles } from './DrawerContent.style'
import { colors } from "../../utils/colors";
import { Icon } from 'react-native-elements'

const DrawerContent = ({ props }) => {
    return (
        <View style={{ flex: 1 }}>
            <AppStatusBar />
            <SafeAreaView style={styles.container}>
                <DrawerContentScrollView {...props}>
                    <View>
                        <Text>Home</Text>
                    </View>
                </DrawerContentScrollView>
                <View style={styles.mainContainer}>
                    <DrawerItem
                        label={'Home'}
                        onPress={() => { props.navigation.navigate('HomeDrawer')}}
                        style={styles.drawerItem}
                        labelStyle={styles.label}
                        icon={({ focused, size}) => (
                            <Icon
                                name='home'
                                type='material'
                                color={focused ? colors.yellowColor : colors.semiDarkGray}
                                size={size}
                            />
                        )}
                        activeBackgroundColor={colors.semiDarkGray}
                    />
                    <DrawerItem
                        label={'Your tickets'}
                        onPress={() => { props.navigation.navigate('Ticket')}}
                        style={styles.drawerItem}
                        labelStyle={styles.label}
                        icon={({ focused, size}) => (
                            <Icon
                                name='albums-outline'
                                type='ionicon'
                                color={focused ? colors.yellowColor : colors.semiDarkGray}
                                size={size}
                            />
                        )}
                        activeBackgroundColor={colors.semiDarkGray}
                    />
                    <DrawerItem
                        label={'Favorite'}
                        onPress={() => {}}
                        style={styles.drawerItem}
                        labelStyle={styles.label}
                        icon={({ focused, size}) => (
                            <Icon
                                name='favorite'
                                type='material'
                                color={focused ? colors.yellowColor : colors.semiDarkGray}
                                size={size}
                            />
                        )}
                        activeBackgroundColor={colors.semiDarkGray}
                    />
                    <DrawerItem
                        label={'Support'}
                        onPress={() => {}}
                        style={styles.drawerItem}
                        labelStyle={styles.label}
                        icon={({ focused, size}) => (
                            <Icon
                                name='help'
                                type='material'
                                color={focused ? colors.yellowColor : colors.semiDarkGray}
                                size={size}
                            />
                        )}
                        activeBackgroundColor={colors.semiDarkGray}
                    />
                    <DrawerItem
                        label={'Profile'}
                        onPress={() => {}}
                        style={styles.drawerItem}
                        labelStyle={styles.label}
                        icon={({ focused, size}) => (
                            <Icon
                                name='account-circle'
                                type='material'
                                color={focused ? colors.yellowColor : colors.semiDarkGray}
                                size={size}
                            />
                        )}
                        activeBackgroundColor={colors.semiDarkGray}
                    />
                </View>
            </SafeAreaView>
        </View>
    )
}

export default DrawerContent;
