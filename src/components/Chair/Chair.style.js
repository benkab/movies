import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: 38,
        height: 38
    },
    chairBaseContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative'
    },
    base: {
        width: 15,
        height: 12,
        borderRadius: 4,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20
    },
    bottomPart: {
        borderRadius: 4,
        width: 24,
        height: 4,
        marginTop: 1,
        borderBottomRightRadius: 100,
        borderBottomLeftRadius: 100
    },
    arm: {
        position: 'absolute',
        width: 4,
        height: 8,
        bottom: 4,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10
    },
    leftArm: {
        left: 7,
    },
    rightArm: {
        right: 7,
    }
});
