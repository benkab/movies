import { connect } from 'react-redux'
import Chair from './Chair'
import { setSeats } from './../../redux/global/actions'

export const mapStateToProps = ({ global, ...state }) => {
    const { seats } = global
    return ({
        ...state,
        seats
    })
}

export const mergeProps = (state, { dispatch }, props) => ({
    ...state,
    ...props,
    onSetSeats (data) {
        dispatch(setSeats(data))
    }
})

export default
connect(mapStateToProps, null, mergeProps)
(Chair)
