import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import { styles } from "./Chair.style";
import { colors } from "../../utils/colors";

const Chair = ({ seat, onSetSeats, seats }) => {
    const { disabled, reserved } = seat

    const pushToArray = (seat) => {
        onSetSeats([...seats, seat])
    }

    const deleteFromArray = (seat) => {
        let arr = []
        seats.forEach((item) => {
            if (item !== seat) {
                arr.push(item)
            }
        })
        onSetSeats(arr)
    }

    const handleSeatTouch = (seat) => {
        if (seats.includes(seat)) {
            deleteFromArray(seat)
        } else {
            pushToArray(seat)
        }
    }

    return (
        <View style={styles.container}>
            {!disabled && (
                <View>
                    {reserved ? (
                        <View style={styles.chairBaseContainer}>
                            <View
                                style={[styles.arm, styles.leftArm, {
                                    backgroundColor: colors.darkGray
                                }]}
                            />
                            <View
                                style={[styles.arm, styles.rightArm, { backgroundColor: colors.darkGray}]}
                            />
                            <View
                                style={[styles.base, { backgroundColor: colors.darkGray}]}
                            />
                            <View
                                style={[styles.bottomPart, { backgroundColor: colors.darkGray}]}
                            />
                        </View>
                    ) : (
                        <TouchableOpacity
                            onPress={() => handleSeatTouch(seat)}
                            style={styles.chairBaseContainer}
                        >
                            <View
                                style={[styles.arm, styles.leftArm, {
                                    backgroundColor:
                                        seats.includes(seat) ?
                                            colors.yellowColor :
                                            colors.lightGray
                                }]}
                            />
                            <View
                                style={[styles.arm, styles.rightArm, {
                                    backgroundColor:
                                        seats.includes(seat) ?
                                            colors.yellowColor :
                                            colors.lightGray
                                }]}
                            />
                            <View
                                style={[styles.base, {
                                    backgroundColor:
                                        seats.includes(seat) ?
                                            colors.yellowColor :
                                            colors.lightGray
                                }]}
                            />
                            <View
                                style={[styles.bottomPart, {
                                    backgroundColor:
                                        seats.includes(seat) ?
                                            colors.yellowColor :
                                            colors.lightGray
                                }]}
                            />
                        </TouchableOpacity>
                    )}

                </View>
            )}
        </View>
    );
}

export default Chair;
