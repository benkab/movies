import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../utils/colors";

const { width } = Dimensions.get('screen')

export const styles = StyleSheet.create({
    outsideContainer: {
        marginBottom: 8
    },
    inputContainer: {
        width: width - 32,
        height: 46,
        backgroundColor: colors.darkBackgroundColor,
        borderRadius: 6,
        paddingHorizontal: 16,
        fontSize: 14,
        fontWeight: '600',
        color: colors.lightGray
    },
});
