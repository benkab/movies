import React from 'react';
import {View, TextInput} from 'react-native';
import { styles } from "./Input.style";
import { colors } from "../../utils/colors";

const Input = ({
    placeholder,
    action,
    autoCapitalize,
    keyboardType,
    onChangeText,
    returnKeyType,
    secureTextEntry
}) => {
    return (
        <View style={styles.outsideContainer}>
            <TextInput
                placeholder={placeholder}
                style={styles.inputContainer}
                placeholderTextColor={colors.darkGray}
                keyboardAppearance={"dark"}
                autoCapitalize={autoCapitalize}
                autoCorrect={false}
                keyboardType={keyboardType}
                onChangeText={(text) => {
                    onChangeText(text)
                }}
                returnKeyType={returnKeyType}
                secureTextEntry={secureTextEntry}
            />
        </View>
    );
}

export default Input;
