import { StyleSheet, Platform } from "react-native";
import { colors } from "../../utils/colors";
import { StatusBar } from "expo-status-bar";

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBar.currentHeight;
export const styles = StyleSheet.create({
    statusBar: {
        height: STATUSBAR_HEIGHT,
        backgroundColor: colors.darkBackgroundColor
    },
});
