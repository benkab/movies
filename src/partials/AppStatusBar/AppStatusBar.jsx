import React from 'react';
import { StatusBar } from "expo-status-bar";
import { colors } from "../../utils/colors";
import { View } from 'react-native';
import { styles } from './AppStatusBar.style'

const AppStatusBar = ({ ...props }) => {
    return (
        <View style={styles.statusBar}>
            <StatusBar
                style="light"
                backgroundColor={colors.statusBarColor}
                {...props}
            />
        </View>
    );
}

export default AppStatusBar;
