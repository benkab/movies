import { StyleSheet } from "react-native";
import { colors } from "../../utils/colors";

export const styles = StyleSheet.create({
    bookButtonContainer: {
        width: 256,
        height: 44,
        backgroundColor: colors.yellowColor,
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: colors.darkBackgroundColor,
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    bookButtonContainerDisabled: {
        backgroundColor: colors.darkYellowColor
    },
    bookButtonText: {
        fontSize: 15,
        fontWeight: '700',
        color: colors.mainColor,
    },
});
