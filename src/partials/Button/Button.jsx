import React from 'react';
import {View, Text, TouchableOpacity, Dimensions} from 'react-native';
import { styles } from "./Button.style";

const { width } = Dimensions.get('screen')

const Button = ({ text, disabled, action, fullWidth }) => {
    return (
        <View>
            {disabled ? (
                <View
                    style={[
                        styles.bookButtonContainer,
                        styles.bookButtonContainerDisabled, {
                            width: fullWidth ? width - 32 : 256
                        }
                    ]}
                >
                    <Text style={styles.bookButtonText}>{text}</Text>
                </View>
            ) : (
                <TouchableOpacity
                    style={[styles.bookButtonContainer, , {
                        width: fullWidth ? width - 32 : 256
                    }]}
                    onPress={() => {action()}}
                >
                    <Text style={styles.bookButtonText}>{text}</Text>
                </TouchableOpacity>
            )}
        </View>
    );
}

export default Button;
