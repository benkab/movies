import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../utils/colors";

const { width, height } = Dimensions.get('screen')

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.mainColor,
    },
    title: {
        fontSize: 22,
        fontWeight: '700',
        color: colors.lightGray,
        paddingRight: 8
    },
    header: {
        fontSize: 15,
        fontWeight: '600',
        color: colors.darkGray,
    },
    description: {
        fontSize: 18,
        fontWeight: '700',
        color: colors.lightGray,
        paddingTop: 4
    },
    seatsContainer: {
        flexDirection: 'row'
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 32,
    }
});
