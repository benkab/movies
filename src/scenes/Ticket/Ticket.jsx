import React from 'react';
import {View, Image, StyleSheet, Dimensions, Text, TouchableOpacity, SafeAreaView} from 'react-native';
import { styles } from "./Ticket.style";
import AppStatusBar from "../../partials/AppStatusBar";
import {SharedElement} from "react-native-shared-element";
import { LinearGradient } from 'expo-linear-gradient';
import { colors } from "../../utils/colors";
import BackButton from "../../components/BackButton";
import Button from "../../partials/Button";
import * as Animatable from 'react-native-animatable';
import {Icon} from "react-native-elements";
const seats = ['A0', 'A1', 'B5']

const ANIMATION_DURATION = 500
const DELAY = 200
const { width, height } = Dimensions.get('screen')
const fadeInBottom = {
    0: {
        opacity: 0,
        translateY: 100
    },
    1: {
        opacity: 1,
        translateY: 0
    }
}
const show = {
    0: {
        opacity: 0,
    },
    1: {
        opacity: 1,
    }
}

const Ticket = ({ navigation, route }) => {
    const { movie } = route.params
    return (
        <View style={styles.container}>
            <AppStatusBar />
            <SafeAreaView style={{ flex: 1, position: 'relative' }}>
                <BackButton navigation={navigation} />
                {movie && (
                    <SharedElement id={`ticket-item-${movie.key}`} style={StyleSheet.absoluteFill}>
                        <Image source={movie.image} style={[StyleSheet.absoluteFill, {
                            width: width,
                            height: height
                        }]}/>
                    </SharedElement>
                )}
                <View
                    style={StyleSheet.absoluteFillObject}
                >
                    <LinearGradient
                        colors={['rgba(29, 29, 39, 0.3)', 'rgba(29, 29, 39, 0.98)', colors.darkBackgroundColor]}
                        style={StyleSheet.absoluteFillObject}
                    />
                </View>
                <Animatable.View
                    animation={fadeInBottom}
                    duration={ANIMATION_DURATION - 300}
                    DELAY={DELAY - 200}
                    style={[StyleSheet.absoluteFillObject, {
                        justifyContent: 'flex-end'
                    }]}>
                    <View style={{
                        paddingVertical: 48,
                        paddingHorizontal: 24
                    }}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.title}>
                                {movie.title}
                            </Text>
                            <Icon
                                name='check-circle'
                                type='material'
                                color={colors.greenColor}
                                size={24}
                            />
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginBottom: 24
                        }}>
                            <View>
                                <Text style={styles.header}>
                                    DATE
                                </Text>
                                <Text
                                    style={styles.description}
                                >
                                    Tue, 15th FEB
                                </Text>
                            </View>
                            <View>
                                <Text style={styles.header}>
                                    TIME
                                </Text>
                                <Text
                                    style={styles.description}
                                >
                                    10:30 AM
                                </Text>
                            </View>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                            <View>
                                <Text style={styles.header}>
                                    SEAT
                                </Text>
                                <View style={styles.seatsContainer}>
                                    {seats.map((seat, index) => {
                                        const condition = index !== 0
                                        return (
                                            <View
                                                key={index}
                                                style={{
                                                    height: 28,
                                                    paddingLeft: condition ? 12 : 0,
                                                    marginLeft: condition ? 12 : 0,
                                                    borderLeftWidth: condition ? 1 : 0,
                                                    borderLeftColor: condition ? colors.borderColor : 'transparent'
                                                }}
                                            >
                                                <Text
                                                    style={styles.description}
                                                >
                                                    {seat}
                                                </Text>
                                            </View>
                                        )
                                    })}
                                </View>
                            </View>
                            <View>
                                <Text style={styles.header}>
                                    GATE
                                </Text>
                                <Text
                                    style={styles.description}
                                >
                                    3
                                </Text>
                            </View>
                        </View>
                    </View>
                </Animatable.View>
            </SafeAreaView>
        </View>
    );
}

Ticket.sharedElements = (route) => {
    const { movie } = route.params
    return [
        {
            id: `ticket-item-${movie.key}`
        }
    ]
}

export default Ticket;
