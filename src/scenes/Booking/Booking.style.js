import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../utils/colors";

const { width, height } = Dimensions.get('screen')

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.mainColor,
    },
    contentContainer: {
        paddingVertical: 54,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    backButtonContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        paddingHorizontal: 16,
    },
    backButton: {
        marginLeft: -4
    },
    movieTitle: {
        fontSize: 26,
        fontWeight: '700',
        color: colors.lightGray
    },
    ratingContainer: {
        marginTop: 4,
        flexDirection: 'row'
    },
    reviews: {
        fontSize: 14,
        fontWeight: '500',
        color: colors.lightGray,
        paddingLeft: 8
    },
    duration: {
        fontSize: 14,
        fontWeight: '600',
        color: colors.lightGray,
        marginTop: 8
    },
    summary: {
        fontSize: 16,
        fontWeight: '600',
        color: colors.lightGray,
        lineHeight: 24,
        marginTop: 24,
    },
    categoriesContainer: {
        marginTop: 16,
        flexDirection: 'row'
    },
    categoryContainer: {
        marginRight: 16,
        paddingHorizontal: 8,
        paddingVertical: 4,
        borderWidth: 1,
        borderColor: colors.darkGray,
        borderRadius: 4
    },
    category: {
        fontSize: 14,
        fontWeight: '600',
        color: colors.darkGray,
    },
    content: {
        paddingHorizontal: 16,
        marginTop: '5%'
    },
    lastContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: height > 750 ? 32 : 24,
        left: 0,
        right: 0
    },
    bookButtonContainer: {
        width: 256,
        height: 44,
        backgroundColor: colors.yellowColor,
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    date: {
        fontSize: 14,
        fontWeight: '700',
        color: colors.yellowColor,
    },
    contentInnerContainer: {
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: width - 32,
        marginBottom: 16,
        flexWrap: 'wrap'
    },
    status: {
        fontSize: 14,
        fontWeight: '700',
        paddingLeft: 8
    },
    availableStatus: {
        color: colors.lightGray
    },
    reservedStatus: {
        color: colors.darkGray
    },
    selectedStatus: {
        color: colors.yellowColor
    },
    circle: {
        width: 8,
        height: 8,
        borderRadius: 100
    },
    availableCircle: {
        backgroundColor: colors.lightGray
    },
    reservedCircle: {
        backgroundColor: colors.darkGray
    },
    selectedCircle: {
        backgroundColor: colors.yellowColor
    },
    statusContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    bookButtonText: {
        fontSize: 16,
        fontWeight: '700',
        color: colors.mainColor,
    },
    trapezoid: {
        width: '100%',
        height: 0,
        borderBottomWidth: 180,
        borderBottomColor: 'rgba(207, 27, 76, 0.05)',
        borderLeftWidth: 50,
        borderLeftColor: 'transparent',
        borderRightWidth: 50,
        borderRightColor: 'transparent',
        borderStyle: 'solid'
    }
});
