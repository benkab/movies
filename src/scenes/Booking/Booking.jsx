import React from 'react';
import {View, Image, StyleSheet, Dimensions, Text, TouchableOpacity, SafeAreaView} from 'react-native';
import { styles } from "./Booking.style";
import AppStatusBar from "../../partials/AppStatusBar";
import {SharedElement} from "react-native-shared-element";
import { LinearGradient } from 'expo-linear-gradient';
import { colors } from "../../utils/colors";
import * as Animatable from 'react-native-animatable';
import Chair from "../../components/Chair";
import seats from "../../constants/seats";
import BackButton from "../../components/BackButton";
import Button from "../../partials/Button";

const ANIMATION_DURATION = 300
const DELAY = 200
const { width, height } = Dimensions.get('screen')
const fadeInBottom = {
    0: {
        opacity: 0,
        translateY: 100
    },
    1: {
        opacity: 1,
        translateY: 0
    }
}
const show = {
    0: {
        opacity: 0,
    },
    1: {
        opacity: 1,
    }
}
const screen = require('./../../assets/images/screen.png')

const Booking = ({ navigation, route }) => {
    const { movie } = route.params
    return (
        <View style={styles.container}>
            <AppStatusBar />
            <SafeAreaView style={{ flex: 1}}>
                {movie && (
                    <SharedElement id={`movie-item-${movie.key}`} style={StyleSheet.absoluteFill}>
                        <Image source={movie.image} style={[StyleSheet.absoluteFill, {
                            width: width,
                            height: height
                        }]}/>
                    </SharedElement>
                )}
                <Animatable.View
                    animation={fadeInBottom}
                    duration={ANIMATION_DURATION - 200}
                    style={StyleSheet.absoluteFillObject}
                >
                    <LinearGradient
                        colors={['rgba(29, 29, 39, 0.3)', 'rgba(29, 29, 39, 0.98)', colors.darkBackgroundColor]}
                        style={StyleSheet.absoluteFillObject}
                    />
                </Animatable.View>
                <View style={[StyleSheet.absoluteFillObject, styles.contentContainer]}>
                    <BackButton navigation={navigation} />
                    <View style={styles.contentInnerContainer}>
                        <View style={styles.content}>
                            <Animatable.Text
                                style={styles.movieTitle}
                                animation={fadeInBottom}
                                duration={ANIMATION_DURATION}
                                delay={DELAY + 200}
                            >
                                {movie.title}
                            </Animatable.Text>
                            <Animatable.View
                                style={styles.ratingContainer}
                                animation={fadeInBottom}
                                duration={ANIMATION_DURATION}
                                delay={DELAY + 300}
                            >
                                <Text style={styles.date}>Friday, 20th</Text>
                                <Text style={styles.reviews}>3:30 PM</Text>
                            </Animatable.View>
                            <View
                                style={{
                                    marginTop: '10%',
                                    flexDirection: 'column',
                                    justifyContent: 'space-between',
                                    paddingHorizontal: 0,
                                    position: 'relative'
                                }}
                            >
                                <Animatable.View
                                    style={{
                                        width: width - 32,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginBottom: 20,
                                    }}
                                    animation={fadeInBottom}
                                    duration={ANIMATION_DURATION}
                                    delay={DELAY + 400}
                                >
                                    <Image
                                        source={screen}
                                        style={{
                                            width: width - 32,
                                            height: 190,
                                            opacity: 0.8,
                                        }}
                                    />
                                </Animatable.View>
                                <Animatable.View
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        position: 'absolute',
                                        left: 0,
                                        right: 0,
                                        top: 70
                                    }}
                                    animation={fadeInBottom}
                                    duration={ANIMATION_DURATION}
                                    delay={DELAY + 450}
                                >
                                    <Text style={{
                                        fontSize: 14,
                                        fontWeight: '700',
                                        color: colors.lightGray
                                    }}>Screen</Text>
                                </Animatable.View>
                                <View style={{
                                    position: 'absolute',
                                    top: 170
                                }}>
                                    <Animatable.View
                                        style={styles.row}
                                        animation={show}
                                        duration={ANIMATION_DURATION}
                                        delay={DELAY + 600}
                                    >
                                        {seats.map((seat, index) => {
                                            return (
                                                <Chair
                                                    seat={seat}
                                                    key={index}
                                                />
                                            )
                                        })}
                                    </Animatable.View>
                                    <Animatable.View
                                        style={{
                                            width: width - 140,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginLeft: (width - 140 - 32) / 4
                                        }}
                                        animation={show}
                                        duration={ANIMATION_DURATION}
                                        delay={DELAY + 700}
                                    >
                                        <View style={styles.statusContainer}>
                                            <View style={[styles.circle, styles.availableCircle]}></View>
                                            <Text style={[styles.status, styles.availableStatus]}>Available</Text>
                                        </View>
                                        <View style={styles.statusContainer}>
                                            <View style={[styles.circle, styles.reservedCircle]}></View>
                                            <Text style={[styles.status, styles.reservedStatus]}>Reserved</Text>
                                        </View>
                                        <View style={styles.statusContainer}>
                                            <View style={[styles.circle, styles.selectedCircle]}></View>
                                            <Text style={[styles.status, styles.selectedStatus]}>Selected</Text>
                                        </View>
                                    </Animatable.View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <Animatable.View
                    style={styles.lastContainer}
                    animation={show}
                    duration={ANIMATION_DURATION}
                    delay={DELAY + 1000}
                >
                    <Button
                        action={() => {
                            navigation.navigate('Tickets', {
                                movie: movie
                            })
                        }}
                        text={'Proceed'}
                    />
                </Animatable.View>
            </SafeAreaView>
        </View>
    );
}

Booking.sharedElements = (route) => {
    return [
        {
            id: 'booking'
        }
    ]
}

export default Booking;
