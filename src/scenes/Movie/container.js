import { connect } from 'react-redux'
import Movie from './Movie'
import {
    setTime,
    setDate,
    setSeats,
    setTickets
} from './../../redux/global/actions'

export const mapStateToProps = ({ global, ...state }) => {
    const { time, date, seats, tickets } = global
    return ({
        ...state,
        time,
        date,
        seats,
        tickets
    })
}

export const mergeProps = (state, { dispatch }, props) => ({
    ...state,
    ...props,
    didMount() {},
    onReset () {
        dispatch(setTime(''))
        dispatch(setDate(''))
        dispatch(setSeats([]))
    },
    onSaveTickets (data) {
        dispatch(setTickets(data))
    }
})

export default
connect(mapStateToProps, null, mergeProps)
(Movie)
