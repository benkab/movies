import React, { useRef } from 'react';
import {View, Image, StyleSheet, Dimensions, Text, TouchableOpacity, SafeAreaView} from 'react-native';
import { styles } from "./Movie.style";
import AppStatusBar from "../../partials/AppStatusBar";
import {SharedElement} from "react-native-shared-element";
import { LinearGradient } from 'expo-linear-gradient';
import { colors } from "../../utils/colors";
import StarRating from "react-native-star-rating";
import * as Animatable from 'react-native-animatable';
import DateChooser from "../../components/DateChooser";
import TimeChooser from "../../components/TimeChooser";
import BackButton from "../../components/BackButton";
import Button from "../../partials/Button";
import Modal from 'react-native-modalbox';
import Chair from "../../components/Chair";
import seatsList from "../../constants/seats";

const ANIMATION_DURATION = 300
const DELAY = 200
const { width, height } = Dimensions.get('screen')
const fadeInBottom = {
    0: {
        opacity: 0,
        translateY: 100
    },
    1: {
        opacity: 1,
        translateY: 0
    }
}
const show = {
    0: {
        opacity: 0,
    },
    1: {
        opacity: 1,
    }
}
const screen = require('./../../assets/images/screen.png')

const Movie = ({
    navigation,
    route,
    time,
    date,
    onReset,
    seats,
    tickets,
    onSaveTickets
}) => {
    const { movie } = route.params
    const seatModal = useRef();

    React.useEffect(() => {
        if (navigation) {
            const unsubscribe = navigation.addListener('focus', () => {
                seatModal.current.close();
            });

            return unsubscribe;
        }
    }, [navigation]);

    return (
        <View style={styles.container}>
            <AppStatusBar />
            <SafeAreaView style={{ flex: 1}}>
                {movie && (
                    <SharedElement
                        id={`movie-item-${movie.key}`}
                        style={[StyleSheet.absoluteFill, {
                            width: width,
                            height: height
                        }]}
                    >
                        <Image
                            source={movie.image}
                            style={[StyleSheet.absoluteFill, {
                                width: width,
                                height: height
                            }]}
                        />
                    </SharedElement>
                )}
                <Animatable.View
                    animation={fadeInBottom}
                    duration={ANIMATION_DURATION - 200}
                    style={StyleSheet.absoluteFillObject}
                >
                    <LinearGradient
                        colors={['rgba(29, 29, 39, 0.3)', 'rgba(29, 29, 39, 0.98)', colors.darkBackgroundColor]}
                        style={StyleSheet.absoluteFillObject}
                    />
                </Animatable.View>
                <View style={[StyleSheet.absoluteFillObject, styles.contentContainer]}>
                   <BackButton
                       navigation={navigation}
                       priorAction={() => onReset()}
                       hasPriorAction
                   />
                    <View style={styles.contentInnerContainer}>
                        <View style={styles.content}>
                            <Animatable.Text
                                style={styles.movieTitle}
                                animation={fadeInBottom}
                                duration={ANIMATION_DURATION}
                                delay={DELAY + 200}
                            >
                                {movie.title}
                            </Animatable.Text>
                            <Animatable.View
                                style={styles.ratingContainer}
                                animation={fadeInBottom}
                                duration={ANIMATION_DURATION}
                                delay={DELAY + 300}
                            >
                                <StarRating
                                    disabled={false}
                                    maxStars={5}
                                    rating={4.2}
                                    starSize={16}
                                    fullStarColor={colors.yellowColor}
                                    emptyStarColor={colors.semiDarkGray}
                                    selectedStar={(rating) => {}}
                                    containerStyle={{
                                        minWidth: 100,
                                        maxWidth: 100,
                                    }}
                                />
                                <Text style={styles.reviews}>24 Reviews</Text>
                            </Animatable.View>
                            <Animatable.Text
                                style={styles.duration}
                                animation={fadeInBottom}
                                duration={ANIMATION_DURATION}
                                delay={DELAY + 400}
                            >
                                1h 24min
                            </Animatable.Text>
                            <Animatable.View
                                style={styles.categoriesContainer}
                                animation={fadeInBottom}
                                duration={ANIMATION_DURATION}
                                delay={DELAY + 500}
                            >
                                <View style={styles.categoryContainer}>
                                    <Text style={styles.category}>Action</Text>
                                </View>
                                <View style={styles.categoryContainer}>
                                    <Text style={styles.category}>Comedy</Text>
                                </View>
                                <View style={styles.categoryContainer}>
                                    <Text style={styles.category}>Adventure</Text>
                                </View>
                            </Animatable.View>
                            <Animatable.Text
                                style={styles.summary}
                                animation={fadeInBottom}
                                duration={ANIMATION_DURATION}
                                delay={DELAY + 600}
                                numberOfLines={5}
                            >
                                {movie.summary}
                            </Animatable.Text>
                        </View>
                        <Animatable.View
                            animation={fadeInBottom}
                            duration={ANIMATION_DURATION}
                            delay={DELAY + 700}
                        >
                            <DateChooser />
                        </Animatable.View>
                    </View>
                </View>
                <Animatable.View
                    style={styles.lastContainer}
                    animation={show}
                    duration={ANIMATION_DURATION}
                    delay={DELAY + 1000}
                >
                    <Button
                        disabled={!date}
                        action={() => {
                            seatModal.current.open();
                            // navigation.navigate('Booking', {
                            //     movie: movie
                            // })
                        }}
                        text={'Select a seat'}
                    />
                </Animatable.View>
                <Modal
                    style={{
                        height: height / 1.2,
                        paddingTop: 32,
                        position: "relative",
                        backgroundColor: colors.darkBackgroundColor,
                        borderTopLeftRadius: 32,
                        borderTopRightRadius: 32
                    }}
                    position={"bottom"}
                    ref={seatModal}
                    backdropOpacity={0.2}
                >
                    <Animatable.Text
                        animation={fadeInBottom}
                        duration={ANIMATION_DURATION}
                        delay={DELAY + 200}
                        style={{
                            color: colors.darkGray,
                            fontSize: 16,
                            fontWeight: '600',
                            paddingBottom: 4,
                            paddingHorizontal: 24
                        }}
                    >Pick your time</Animatable.Text>
                    <Animatable.View
                        animation={fadeInBottom}
                        duration={ANIMATION_DURATION}
                        delay={DELAY + 400}
                        style={{
                            paddingBottom: 32
                        }}
                    >
                        <TimeChooser />
                    </Animatable.View>
                    <Animatable.Text
                        animation={fadeInBottom}
                        duration={ANIMATION_DURATION}
                        delay={DELAY + 600}
                        style={{
                            color: colors.darkGray,
                            fontSize: 16,
                            fontWeight: '600',
                            paddingBottom: 0,
                            paddingHorizontal: 24
                        }}
                    >Select your seat</Animatable.Text>
                    <Animatable.View
                        style={{
                            width: width,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginBottom: -80,
                            marginTop: -16
                        }}
                        animation={fadeInBottom}
                        duration={ANIMATION_DURATION}
                        delay={DELAY + 800}
                    >
                        <Image
                            source={screen}
                            style={{
                                width: width - 32,
                                height: 190,
                                opacity: 0.8,
                            }}
                        />
                    </Animatable.View>
                    <Animatable.View
                        style={styles.row}
                        animation={show}
                        duration={ANIMATION_DURATION + 200}
                        delay={DELAY + 1000}
                    >
                        {seatsList.map((seat, index) => {
                            return (
                                <Chair
                                    seat={seat}
                                    key={index}
                                />
                            )
                        })}
                    </Animatable.View>
                    <Animatable.View
                        style={styles.lastContainer}
                        animation={show}
                        duration={ANIMATION_DURATION + 200}
                        delay={DELAY + 1200}
                    >
                        <Button
                            disabled={!(seats.length > 0 && time)}
                            action={() => {
                                onSaveTickets(
                                    [...tickets, {
                                        movie,
                                        date,
                                        time,
                                        seats
                                    }]
                                )
                                onReset()
                                navigation.navigate('Tickets')
                            }}
                            text={'Reserve'}
                        />
                    </Animatable.View>
                </Modal>
            </SafeAreaView>
        </View>
    );
}

Movie.sharedElements = (route) => {
    const { movie } = route.params

    return [
        {
            id: `movie-item-${movie.key}`
        }
    ]
}

export default Movie;
