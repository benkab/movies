import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../utils/colors";

const { width, height } = Dimensions.get('screen')

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.mainColor,
        position: 'relative'
    },
    contentContainer: {
        paddingVertical: 0,
    },
    movieTitle: {
        fontSize: 26,
        fontWeight: '700',
        color: colors.lightGray
    },
    ratingContainer: {
        marginTop: 4,
        flexDirection: 'row',
        alignItems: 'center'
    },
    reviews: {
        fontSize: 14,
        fontWeight: '500',
        color: colors.semiDarkGray,
        paddingLeft: 8
    },
    duration: {
        fontSize: 14,
        fontWeight: '600',
        color: colors.lightGray,
        marginTop: 8
    },
    summary: {
        fontSize: 16,
        fontWeight: '600',
        color: colors.lightGray,
        lineHeight: 24,
        marginTop: 24
    },
    categoriesContainer: {
        marginTop: 16,
        flexDirection: 'row'
    },
    categoryContainer: {
        marginRight: 16,
        paddingHorizontal: 8,
        paddingVertical: 4,
        borderWidth: 1,
        borderColor: colors.semiDarkGray,
        borderRadius: 4
    },
    category: {
        fontSize: 14,
        fontWeight: '600',
        color: colors.semiDarkGray,
    },
    content: {
        paddingHorizontal: 16,
        marginTop: '30%'
    },
    lastContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: height > 750 ? 32 : 24,
        left: 0,
        right: 0
    },
    bookButtonContainer: {
        width: 256,
        height: 44,
        backgroundColor: colors.yellowColor,
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bookButtonText: {
        fontSize: 16,
        fontWeight: '700',
        color: colors.mainColor,
    },
    contentInnerContainer: {
        flexDirection: 'column',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: width - 48,
        marginBottom: 16,
        flexWrap: 'wrap',
        marginLeft: 24
    },
});
