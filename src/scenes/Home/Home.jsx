import React from 'react';
import {View, ScrollView, SafeAreaView} from 'react-native';
import { styles } from "./Home.style";
import AppStatusBar from "../../partials/AppStatusBar";
import Header from "../../components/Header";
import Title from '../../components/Title'
import Carousel from '../../components/Carousel'
import { SharedElement } from "react-native-shared-element";
import SearchTextInput from "../../components/SearchTextInput";
import List from '../../components/List'

const Home = ({ navigation }) => {

    const onSearching = () => {
        navigation.push('Search')
    }

    return (
        <View style={{ flex: 1 }}>
            <AppStatusBar />
            <SafeAreaView
                style={styles.container}
            >
                <Header navigation={navigation}>
                    <SharedElement
                        id='SearchContainer'
                    >
                        <SearchTextInput
                            fullWidth
                            onSearching={() => onSearching()}
                            animateable
                        />
                    </SharedElement>
                </Header>
                <ScrollView
                    style={{ flex: 1}}
                    horizontal={false}
                    showsVerticalScrollIndicator={false}
                >
                    <Title title="Now showing" navigation={navigation} />
                    <Carousel navigation={navigation} />
                    <Title title="Coming soon" navigation={navigation} />
                    <List navigation={navigation} />
                </ScrollView>
            </SafeAreaView>
        </View>
    );
}

export default Home;
