import { StyleSheet } from "react-native";
import { colors } from "../../utils/colors";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.mainColor,
    },
    moviesContainer: {
        paddingHorizontal: 24,
        paddingVertical: 24,
    }
});
