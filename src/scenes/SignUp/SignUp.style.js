import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../utils/colors";

const { width, height } = Dimensions.get('screen')

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.mainColor,
        padding: 16,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 32
    },
    linksContainer: {
        marginTop: 4,
        marginBottom: 16,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    logo: {
        width: 128,
        height: 128,
    },
    logoContainer: {
        paddingTop: height / 10,
        alignItems: 'center'
    },
    link: {
        fontWeight: '600',
        fontSize: 14,
        color: colors.lightGray
    },
    linkText: {
        fontWeight: '600',
        fontSize: 14,
        color: colors.darkGray,
        paddingRight: 4
    },
    buttonContainer: {
        paddingVertical: 8
    },
    sceneTitle: {
        textAlign: 'center',
        paddingTop: 32,
        fontWeight: '700',
        fontSize: 28,
        color: colors.yellowColor,
    },
    sceneDescription: {
        textAlign: 'center',
        maxWidth: width - 90,
        marginTop: 16,
        fontSize: 14,
        fontWeight: '600',
        color: colors.darkGray,
        lineHeight: 20
    }
});
