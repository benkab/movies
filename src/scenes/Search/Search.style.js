import { StyleSheet } from "react-native";
import { colors } from "../../utils/colors";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.mainColor,
    },
    searchContainer: {
        paddingHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 52
    },
    backButton: {
        height: 42,
        justifyContent: 'center',
        paddingTop: 10,
        marginLeft: -4
    }
});
