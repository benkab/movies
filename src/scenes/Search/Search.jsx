import React from 'react';
import {View, TouchableWithoutFeedback, Keyboard, ScrollView, TouchableOpacity} from 'react-native';
import { styles } from "./Search.style";
import AppStatusBar from "../../partials/AppStatusBar";
import SearchTextInput from "../../components/SearchTextInput";
import { Icon } from 'react-native-elements'
import { colors } from "../../utils/colors";
import { SharedElement } from "react-native-shared-element";

const Search = ({ navigation }) => {

    return (
        <View style={{ flex: 1 }}>
            <AppStatusBar />
            <View style={styles.container}>
                <View style={styles.searchContainer}>
                    <TouchableOpacity
                        style={styles.backButton}
                        onPress={() => navigation.goBack() }
                    >
                        <Icon
                            name='corner-up-left'
                            type='feather'
                            color={colors.lightGray}
                            size={28}
                        />
                    </TouchableOpacity>
                    <SharedElement
                        id='SearchContainer'
                    >
                        <SearchTextInput navigation={navigation} />
                    </SharedElement>
                </View>
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <ScrollView style={{ flex: 1}}>

                    </ScrollView>
                </TouchableWithoutFeedback>
            </View>
        </View>

    );
}

export default Search;
