import { connect } from 'react-redux'
import Tickets from './Tickets'

export const mapStateToProps = ({ global, ...state }) => {
    const { tickets } = global
    return ({
        ...state,
        tickets
    })
}

export default
connect(mapStateToProps, null, null)
(Tickets)
