import { StyleSheet } from "react-native";
import { colors } from "../../utils/colors";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.mainColor,
        justifyContent: 'center'
    },
    content: {},
    circle: {
        position: 'absolute',
        width: 32,
        height: 32,
        backgroundColor: colors.mainColor,
        borderRadius: 100,
        bottom: 62,
        zIndex: 1
    },
    leftCircle: {
        left: -16,
    },
    rightCircle: {
        right: -16,
    },
    title: {
        fontSize: 22,
        fontWeight: '700',
        color: colors.lightGray,
        paddingRight: 8
    },
    header: {
        fontSize: 15,
        fontWeight: '600',
        color: colors.darkGray,
    },
    description: {
        fontSize: 18,
        fontWeight: '700',
        color: colors.lightGray,
        paddingTop: 4
    },
    seatsContainer: {
        flexDirection: 'row'
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 32,
    },
    sliderIndicator: {
        height: 6,
        width: 6,
        backgroundColor: colors.darkGray,
        marginRight: 8,
        borderRadius: 100
    },
    activeSliderIndicator: {
        transform: [
            {
                scale: 1.05
            }
        ],
        backgroundColor: colors.yellowColor
    }

});
