import React, {useRef, useState} from 'react';
import {
    Animated,
    Image,
    TouchableOpacity,
    View,
    Text,
    Dimensions,
    SafeAreaView, StyleSheet,
} from 'react-native';
import { styles } from "./Tickets.style";
import AppStatusBar from "../../partials/AppStatusBar";
import BackButton from "../../components/BackButton";
import { colors } from "../../utils/colors";
import {LinearGradient} from "expo-linear-gradient";
import { Icon } from 'react-native-elements'
import { SharedElement } from "react-native-shared-element";
import moment from 'moment'
import * as Animatable from 'react-native-animatable';

const { width, height } = Dimensions.get('screen')

const ITEM_WIDTH = width - 32
const SPACER_WIDTH = (width - ITEM_WIDTH)
const fadeInBottom = {
    0: {
        opacity: 0,
    },
    1: {
        opacity: 1,
    }
}


const Ticket = ({ ticket, scaleY, navigation }) => {
    const { movie, date, time, seats } = ticket
    return (
        <View style={{
            width: ITEM_WIDTH,
            marginBottom: 48,
            paddingTop: 64
        }}>
            <TouchableOpacity
                onPress={() => {
                    navigation.navigate('Ticket', {
                        movie: movie
                    })
                }}
                activeOpacity={0.9}
                style={{
                    width: width - 64,
                    height: height * 0.65,
                    backgroundColor: colors.darkBackgroundColor,
                    borderRadius: 30,
                    position: 'relative',
                    overflow: 'hidden',
                    padding: 16,
                    transform: [{ scale: scaleY }]
                }}
            >
                <SharedElement id={`ticket-item-${movie.key}`}>
                    <Image source={movie.image} style={{
                        width: '100%',
                        height: '100%',
                        borderRadius: 20,
                    }} />
                </SharedElement>
                <LinearGradient
                    colors={[
                        'rgba(29, 29, 39, 0.1)',
                        'rgba(29, 29, 39, 0.98)',
                        colors.darkBackgroundColor
                    ]}
                    style={StyleSheet.absoluteFillObject}
                />
                <View style={[styles.circle, styles.leftCircle]} />
                <View style={[styles.circle, styles.rightCircle]} />
                <View
                    style={[StyleSheet.absoluteFillObject, {
                        justifyContent: 'flex-end'
                    }]}>
                    <View style={{
                        padding: 32
                    }}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.title}>
                                {movie.title}
                            </Text>
                            <Icon
                                name='check-circle'
                                type='material'
                                color={colors.greenColor}
                                size={24}
                            />
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginBottom: 24
                        }}>
                            <View>
                                <Text style={styles.header}>
                                    DATE
                                </Text>
                                <Text
                                    style={styles.description}
                                >
                                    {`${moment(new Date(date)).format('ddd')}, ${moment(new Date(date)).format('Do')} `}
                                    <Text>
                                        {moment(new Date(date)).format('MMM')}
                                    </Text>
                                </Text>
                            </View>
                            <View>
                                <Text style={styles.header}>
                                    TIME
                                </Text>
                                <Text
                                    style={styles.description}
                                >
                                    {time.time}
                                </Text>
                            </View>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                            <View>
                                <Text style={styles.header}>
                                    SEAT
                                </Text>
                                <View style={styles.seatsContainer}>
                                    {seats.map((seat, index) => {
                                        const condition = index !== 0
                                        return (
                                            <View
                                                key={index}
                                                style={{
                                                    height: 28,
                                                    paddingLeft: condition ? 12 : 0,
                                                    marginLeft: condition ? 12 : 0,
                                                    borderLeftWidth: condition ? 1 : 0,
                                                    borderLeftColor: condition ? colors.borderColor : 'transparent'
                                                }}
                                            >
                                                <Text
                                                    style={styles.description}
                                                >
                                                    {seat.description}
                                                </Text>
                                            </View>
                                        )
                                    })}
                                </View>
                            </View>
                            <View>
                                <Text style={styles.header}>
                                    GATE
                                </Text>
                                <Text
                                    style={styles.description}
                                >
                                    3
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{
                        paddingHorizontal: 24,
                        height: 80,
                        borderWidth: 1,
                        borderColor: colors.ticketBorderGray,
                        borderStyle: 'dashed',
                        borderRadius: 1,
                        width: '110%',
                        marginLeft: '-5%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginBottom: -1
                    }}>
                        {/*<Barcode value="Hello World" format="CODE128" />;*/}
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const Tickets = ({ navigation, tickets }) => {
    const [data, setData] = useState([{key: 'left-spacer'}, ...tickets.reverse(), {key: 'right-spacer'}])
    const scrollX = useRef(new Animated.Value(0)).current
    const [activeIndex, setActiveIndex] = useState(0)

    return (
        <View style={{ flex: 1 }}>
            <AppStatusBar />
            <SafeAreaView
                style={styles.container}
            >
                <BackButton navigation={navigation} />
                {data.length > 0 && (
                    <Animatable.View
                        style={styles.content}
                        animation={fadeInBottom}
                        duration={400}
                        delay={300}
                    >
                        <Animated.FlatList
                            data={data}
                            keyExtractor={({ item, i }) => i}
                            horizontal
                            contentContainerStyle={{
                                alignItems: 'center',
                            }}
                            scrollEnabled={true}
                            renderItem={({item, index : i}) => {
                                if (item && !item.movie) {
                                    return (
                                        <View style={{ width: SPACER_WIDTH }}/>
                                    )
                                }
                                const inputRange = [
                                    (i - 2) * ITEM_WIDTH,
                                    (i - 1) * ITEM_WIDTH,
                                    i * ITEM_WIDTH
                                ]
                                const scaleY = scrollX.interpolate({
                                    inputRange,
                                    outputRange: [0.75, 1.05, 0.5],
                                    extrapolate: 'clamp'
                                })
                                return (
                                    <Ticket
                                        ticket={item}
                                        key={i}
                                        index={i + 1}
                                        scaleY={scaleY}
                                        navigation={navigation}
                                    />
                                )
                            }}
                            showsHorizontalScrollIndicator={false}
                            snapToInterval={ITEM_WIDTH}
                            decelerationRate={Platform.OS === 'ios' ? 0 : 0.9}
                            bounces={false}
                            onScroll={Animated.event(
                                [{ nativeEvent: { contentOffset: { x: scrollX }}}],
                                { useNativeDriver: true}
                            )}
                            scrollEventThrottle={16}
                            renderToHardwareTextureAndroid
                            snapToAlignment='start'
                        />
                    </Animatable.View>
                )}
            </SafeAreaView>
        </View>
    );
}

export default Tickets;
