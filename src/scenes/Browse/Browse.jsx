import React, {useRef, useState} from 'react';
import {
    Animated,
    FlatList,
    Image,
    TouchableOpacity,
    View,
    Text,
    Dimensions,
    StyleSheet,
    SafeAreaView,
} from 'react-native';
import { styles } from "./Browse.style";
import AppStatusBar from "../../partials/AppStatusBar";
import movies from "../../constants/movies";
import MaskedView from "@react-native-community/masked-view";
import Svg, { Rect } from "react-native-svg";
import { LinearGradient } from 'expo-linear-gradient';
import StarRating from "react-native-star-rating";
import { colors } from "../../utils/colors";
import * as Animatable from 'react-native-animatable';
import BackButton from "../../components/BackButton";

const { width, height } = Dimensions.get('screen')
const ITEM_WIDTH = (width * 0.57) + 32
const SPACER_WIDTH = (width - ITEM_WIDTH) / 2
const BACKDROP_HEIGHT = height * .6

const AnimatedSvg = Animated.createAnimatedComponent(Svg)

const fadeInBottom = {
    0: {
        opacity: 0,
    },
    1: {
        opacity: 1,
    }
}

const Browse = ({ navigation }) => {

    const [data, setData] = useState([{key: 'left-spacer'}, ...movies, {key: 'right-spacer'}])
    const scrollX = useRef(new Animated.Value(0)).current

    const CarouselItem = ({ item, translateY, index }) => {
        return (
            <Animatable.View
                animation={fadeInBottom}
                duration={400}
                delay={300 * index}
            >
                <Animated.View style={[styles.carouselItem, {
                    transform: [{ translateY }]
                }]}>
                    <Text style={styles.title}>{item.title}</Text>
                    <View style={styles.ratingContainer}>
                        <StarRating
                            disabled={false}
                            maxStars={5}
                            rating={item.rate}
                            starSize={16}
                            fullStarColor={colors.yellowColor}
                            selectedStar={(rating) => {}}
                            containerStyle={{
                                minWidth: 100,
                                maxWidth: 100,
                            }}
                        />
                    </View>
                    <Text style={styles.year}>{item.category} | Romance | {item.year}</Text>
                    <TouchableOpacity
                        activeOpacity={.9}
                        style={styles.imageContainer}
                        onPress={() => {
                            navigation.navigate('Movie', {
                                movie: item
                            })
                        }}
                    >
                        <Image source={item.image} style={styles.carouselImage} />
                    </TouchableOpacity>
                </Animated.View>
            </Animatable.View>
        )
    }

    const Backdrop = ({ movies, scrollX }) => {
        return (
            <View style={[StyleSheet.absoluteFillObject, {
                width: width,
                height: BACKDROP_HEIGHT,
            }]}>
                <FlatList
                    data={movies}
                    keyExtractor={(item) => item.key + '-backdrop'}
                    removeClippedSubviews={false}
                    contentContainerStyle={{width, height: BACKDROP_HEIGHT}}
                    renderItem={({ item, index: backdropIndex}) => {
                        if (!item.image) {
                            return null
                        }
                        const inputRange = [
                            (backdropIndex - 2) * ITEM_WIDTH,
                            (backdropIndex - 1) * ITEM_WIDTH
                        ]
                        const translateX = scrollX.interpolate({
                            inputRange,
                            outputRange: [-width, 0]
                        })
                        return (
                            <MaskedView
                                style={StyleSheet.absoluteFillObject}
                                maskElement={
                                    <Animated.View
                                        removeClippedSubviews={false}
                                        style={{
                                            position: 'absolute',
                                            width: width,
                                            height: height,
                                            overflow: 'hidden',
                                            transform: [
                                                { translateX: translateX }
                                            ],
                                            zIndex: 2
                                        }}
                                    >
                                        <Image
                                            source={item.image}
                                            style={{
                                                width: width,
                                                height: height,
                                                position: 'absolute',
                                                zIndex: 1
                                            }}
                                        />
                                    </Animated.View>
                                }
                            >
                                <Image
                                    source={item.image}
                                    style={{
                                        width: width,
                                        height: BACKDROP_HEIGHT,
                                        resizeMode: 'cover'
                                    }}
                                />
                            </MaskedView>
                        )
                    }}
                />
            </View>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <AppStatusBar />
            <SafeAreaView
                style={{
                    flex: 1,
                    position: 'relative'
                }}
            >
                <BackButton navigation={navigation} />
                <Backdrop movies={movies} scrollX={scrollX} />
                <View style={[StyleSheet.absoluteFillObject, styles.container]}>
                    <Animated.FlatList
                        data={data}
                        keyExtractor={(item) => item.key}
                        horizontal
                        contentContainerStyle={{
                            alignItems: 'center',
                        }}
                        scrollEnabled={true}
                        renderItem={({item, index : i}) => {
                            if (!item.image) {
                                return (
                                    <View style={{ width: SPACER_WIDTH }}/>
                                )
                            }
                            const inputRange = [
                                (i - 2) * ITEM_WIDTH,
                                (i - 1) * ITEM_WIDTH,
                                i * ITEM_WIDTH
                            ]
                            const translateY = scrollX.interpolate({
                                inputRange,
                                outputRange: [0, -50, 0],
                                extrapolate: 'clamp'
                            })
                            return (
                                <CarouselItem
                                    item={item}
                                    key={i}
                                    index={i + 1}
                                    translateY={translateY}
                                />
                            )
                        }}
                        showsHorizontalScrollIndicator={false}
                        snapToInterval={ITEM_WIDTH}
                        decelerationRate={Platform.OS === 'ios' ? 0 : 0.9}
                        bounces={false}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { x: scrollX }}}],
                            { useNativeDriver: true}
                        )}
                        scrollEventThrottle={16}
                        renderToHardwareTextureAndroid
                        snapToAlignment='start'
                    />
                </View>
                <View style={styles.background} />
                <LinearGradient
                    colors={['rgba(29, 29, 39, 0.3)', 'rgba(29, 29, 39, 0.98)', colors.darkBackgroundColor]}
                    style={[StyleSheet.absoluteFillObject, {
                        zIndex: 1
                    }]}
                />
            </SafeAreaView>
        </View>

    );
}

export default Browse;
