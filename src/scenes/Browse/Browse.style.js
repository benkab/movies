import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../utils/colors";

const { width, height } = Dimensions.get('screen')
const ITEM_WIDTH = width * 0.57
const ITEM_HEIGHT = height * 0.37

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        zIndex: 2
    },
    background: {
      position: 'absolute',
      left: 0,
        right: 0,
        bottom: 0,
        height: height,
        backgroundColor: colors.mainColor,
        zIndex: -1
    },
    carouselItem: {
        width: ITEM_WIDTH + 32,
    },
    imageContainer: {
        width: ITEM_WIDTH + 32,
        height: ITEM_HEIGHT + 32,
        overflow: 'hidden',
        marginLeft: 0,
        backgroundColor: 'rgba(29, 29, 39, 0.3)',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20
    },
    carouselImage: {
        width: ITEM_WIDTH,
        height: ITEM_HEIGHT,
        borderRadius: 20
    },
    title: {
        textAlign: 'center',
        marginBottom: 8,
        fontSize: 22,
        fontWeight: '600',
        color: colors.lightGray
    },
    year: {
        textAlign: 'center',
        fontSize: 14,
        fontWeight: '600',
        color: colors.semiDarkGray,
        marginBottom: 24
    },
    ratingContainer: {
        marginBottom: 16,
        alignItems: 'center'
    },
    summary: {
        textAlign: 'center',
        fontSize: 14,
        fontWeight: '600',
        color: colors.lightGray,
        lineHeight: 22,
        paddingHorizontal: 24,
        marginTop: 8
    },
    backButtonContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        paddingHorizontal: 16,
        position: 'absolute',
        top: 54,
        left: 0,
        right: 0,
        zIndex: 9999999
    },
    backButton: {
        marginLeft: -4
    },
});
