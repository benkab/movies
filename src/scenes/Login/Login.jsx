import React from 'react';
import {
    View,
    SafeAreaView,
    TouchableWithoutFeedback,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    Image,
    TouchableOpacity,
    Text
} from 'react-native';
import { styles } from "./../SignUp/SignUp.style";
import AppStatusBar from "../../partials/AppStatusBar";
import Button from "../../partials/Button";
import Input from "../../partials/Input"

const logo = require('./../../assets/images/logo.png')

const Login = ({ navigation }) => {

    return (
        <View style={{ flex: 1 }}>
            <AppStatusBar />
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : "height"}
                style={{ flex: 1 }}
            >
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <SafeAreaView
                        style={styles.container}
                    >
                        <View style={styles.logoContainer}>
                            <Image source={logo} style={styles.logo} />
                            <Text style={styles.sceneDescription}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, ratione?</Text>
                        </View>
                        <View>
                            <Input
                                placeholder="Your address email"
                                autoCapitalize={"none"}
                                keyboardType={"email-address"}
                                onChangeText={(text) => {
                                    console.log('text', text)
                                }}
                                returnKeyType={"next"}
                                secureTextEntry={false}
                            />
                            <Input
                                placeholder="Your password"
                                autoCapitalize={"none"}
                                keyboardType={"default"}
                                onChangeText={(text) => {
                                    console.log('text', text)
                                }}
                                returnKeyType={"done"}
                                secureTextEntry={true}
                            />
                            <View style={styles.buttonContainer}>
                                <Button
                                    disabled
                                    fullWidth
                                    action={() => {}}
                                    text={'Log into your account'}
                                />
                            </View>
                            <View style={styles.linksContainer}>
                                <Text style={styles.linkText}>I don't have an account</Text>
                                <TouchableOpacity onPress={() => {
                                    navigation.navigate('SignUp')
                                }}>
                                    <Text style={styles.link}>Sign up</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </SafeAreaView>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </View>
    );
}

export default Login;
