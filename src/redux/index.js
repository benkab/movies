import { combineReducers } from 'redux'
import global from './global/reducers'

export const store = combineReducers({
    global,
})
