const REQUEST_TYPES = {
  REQUEST: 'request',
  SUCCESS: 'success',
  FAILURE: 'failure',
  PENDING: 'pending'
}

export const reducerFactory = (initialState, types) => (state = initialState, { type, payload }) => {
  if (type in types && type.includes(REQUEST_TYPES.RESET)) return { ...initialState }
  else if (type in types && (type.includes(REQUEST_TYPES.REQUEST) || type.includes(REQUEST_TYPES.PENDING))) {
    return {
      ...state,
      loading: true,
      success: false,
      error: undefined
    }
  } else if (type in types && type.includes(REQUEST_TYPES.SUCCESS)) {
    return { ...state, ...payload, loading: false, success: true }
  } else if (type in types && type.includes(REQUEST_TYPES.FAILURE)) {
    return { ...state, loading: false, success: false, error: payload }
  } else if (type in types) return { ...state, ...payload }
  return { ...state }
}
