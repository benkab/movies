import { actionFactory } from './../factories'

export const SET_TIME = 'SET_TIME'
export const setTime = (payload) =>
    actionFactory(SET_TIME, {time: payload})

export const SET_DATE = 'SET_DATE'
export const setDate = (payload) =>
    actionFactory(SET_DATE, {date: payload})

export const SET_SEATS = 'SET_SEATS'
export const setSeats = (payload) =>
    actionFactory(SET_SEATS, {seats: payload})

export const SET_TICKETS = 'SET_TICKETS'
export const setTickets = (payload) =>
    actionFactory(SET_TICKETS, {tickets: payload})
