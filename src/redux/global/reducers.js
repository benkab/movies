import { reducerFactory } from './../factories'
import * as actions from './actions'

export const initialState = {
    date: '',
    time: '',
    tickets: [],
    seats: []
}

/* eslint-disable */
const global = reducerFactory(initialState, actions)
export default global
